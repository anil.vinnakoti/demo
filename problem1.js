function problem1(inventory, id = 33){
    let carDetails;
    for(let i=0;i<inventory.length;i++){
        if(inventory[i]['id'] === id){
            carDetails =(inventory[i]);
            break;
        }
        else {
            carDetails = `car with id ${id} was not found`
        }
    }

    carDetails['id'] === id ? console.log(`Car ${id} is a ${carDetails['car_year']} ${carDetails['car_make']} ${carDetails['car_model']}`) : console.log(carDetails)
}

module.exports = problem1;

